# Jira-dashboard-launchpad

One purpose: see your tickets on a Launchpad Mini

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need to retrieve node modules

```
npm install
```

### Starting the magic

Just launch :

```
npm start
```

And follow what prompts says.

Or type :

```
npm start <THE_JIRA_URL>&os_username=<YOUR_USERNAME>&os_password=<YOUR_PASSWORD>
```
