// ==UserScript==
// @name         Launchpad
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://projects.dsi.ovh/issues/?filter=10924
// @grant        none
// @require      https://raw.githubusercontent.com/LostInBrittany/launchpad-webmidi/master/dist/launchpad-webmidi.umd.js
// @require      https://raw.githubusercontent.com/lodash/lodash/4.17.11-npm/lodash.min.js
// ==/UserScript==

(function() {
  'use strict';

  console.clear();

  let pad = new Launchpad();

  const issueStatus = [
      {
        status: "Backlog",
        column: 0,
        columnColor: pad.red,
      },
      {
        status: "Open",
        column: 1,
        columnColor: pad.red,
      },
      {
        status: "In Progress",
        column: 2,
        columnColor: pad.amber.level(2),
      },
      {
        status: "Need Work",
        column: 4,
        columnColor: pad.amber,
      },
      {
        status: "Blocked",
        column: 5,
        columnColor: pad.yellow,
      },
      {
        status: "Need Information",
        column: 6,
        columnColor: pad.amber,
      },
      {
        status: "Quality Check",
        column: 7,
        columnColor: pad.green,
      },
      {
        status: "Done",
        column: 8,
      },
      {
        status: "To Prod",
        column: 8,
      }
    ],
    issuePriority = [
      {
        status: "Blocker",
        color: pad.red,
      },
      {
        status: "Critical",
        color: pad.red,
      },
      {
        status: "Highest",
        color: pad.red,
      },
      {
        status: "High",
        color: pad.red,
      },
      {
        status: "Major",
        color: pad.red,
      },
      {
        status: "Medium",
        color: pad.amber,
      },
      {
        status: "Minor",
        color: pad.green,
      },
      {
        status: "Low",
        color: pad.green,
      },
      {
        status: "Lowest",
        color: pad.green,
      },
      {
        status: "Trivial",
        color: pad.green,
      }
    ];

  let issues = [];

  pad.connect().then(() => {
    init(pad);

  $(".issuerow").each((item, obj) => {
    issues.push({
    status: $(obj).find(".status>span").text(),
    priority: $(obj).find(".priority>a+img").attr("alt"),
    link: $(obj).find(".issue-link").attr("href"),
    assignee: $(obj).find(".assignee").text()
  })
});

  print(issues);

  createLoading();

  let pressDuration = [];
  pad.on("key", k => {
    switch (k.y) {
  case 8: // column buttons
    break;
  default:
    let issue = _.find(issues, { x: k.x, y:k.y });
    if (!k.pressed && issue) {
      window.open("https://projects.dsi.ovh" + issue.link, "_blank");
    }
    break;
  }
});
});

  function print(issues) {
    let xy = "",
      row = [],
      currentRow = 0,
      currentCol = 0;

    issues = _.chain(issues)
      .map(issue => {
      let i = issue;
    i.status = _.find(issueStatus, {status: i.status});
    i.priority = _.find(issuePriority, {status: i.priority});
    return i;
  }).sortBy([
      "status.column",
      a => _.findIndex(issuePriority, ["status", a.priority.status])
  ]).map(issue => {
      let col = issue.status.column;
    row[col] = row[col] || 0;
    if (row[col] < 8) { // too much issues in this column!
      currentCol = col;
      currentRow = row[col];

      issue.y = currentRow;
      issue.x = currentCol;

      if (currentCol < col + 2) {
        row[col] = row[col] + 1;
      }

      setTimeout(() => {
        const intensity = issue.assignee === "Francois Eoche"? 3: 1;
      paint(issue.priority.color.level(intensity), [issue.x, issue.y]);
    }, 30 * (issue.x + issue.y));
    }
  }).value();
  }

  function paint(color, xy) {
    const intensities = ["low", "medium", "full"];
    pad.col(color, xy);
  }

  function init() {
    reset(pad).then(() => {
      let r = 0;
    let i = setInterval(() => {
      if (issues.length) {
      clearInterval(i);
    } else {
      let colors = [];
      for (let j = 0; j < 9; j++) {
        for (let k = 0; k < 9; k++) {
          colors.push([
            (j + r) % 9,
            k,
            j === (9 - k) % 9 ? pad.off : pad.green
          ]);
        }
      }
      pad.setColors(colors);
      r++;
    }
  }, 100);
  });
  }

  function reset(fullReset) {
    let promises = []
    for (let x = 0; x < 9; x++) {
      for (let y = 0; y < (fullReset ? 9 : 8); y++) {
        promises.push(new Promise((resolve,reject) => setTimeout(() => {
          pad.col(pad.off, [x, y]);
        resolve();
      }, 29 * (x + y))));
      }
    }
    return Promise.all(promises).then(() => Promise.resolve());
  }

  function initColumns() {
    pad.col(pad.off, [
      [0, 8],
      [1, 8],
      [2, 8],
      [3, 8],
      [4, 8],
      [5, 8],
      [6, 8],
      [7, 8]
    ]);
    issueStatus.forEach((i, key) => {
      if (i.columnColor) {
      paint(i.columnColor, [i.column, 8]);
    }
  });
  }

  function createLoading() {
    let timeout = 1000;
    [pad.green, pad.amber, pad.red].forEach(color => {
      for(let col = 0; col < 8; col++) {
      setTimeout(() => {
        paint(color, [col, 8]);
      col++;
    }, timeout);
      timeout += 20000/8;
    }
  });
  }

  function exitWithError() {
    pad.reset(0);
    pad.col(
      pad.red,
      pad.fromMap(
        "x-x--x-xo" +
        "-x----x-o" +
        "x-x--x-xo" +
        "--------o" +
        "--------o" +
        "--xxxx--o" +
        "-x----x-o" +
        "--------o" +
        "oooooooo "
      )
    );
  }

  setTimeout(() => window.location.reload(1), 60000);
})();
