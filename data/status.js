const Launchpad = require("launchpad-mini");

module.exports = [
  {
    status: "open",
    column: 0,
    columnColor: Launchpad.Colors.red,
    hex: "\x1b[34m"
  },
  {
    status: "backlog",
    column: 0,
    columnColor: Launchpad.Colors.red,
    hex: "\x1b[34m"
  },
  {
    status: "blocked",
    column: 2,
    columnColor: Launchpad.Colors.yellow,
    hex: "\x1b[33m"
  },
  {
    status: "in progress",
    column: 4,
    columnColor: Launchpad.Colors.amber.medium,
    hex: "\x1b[33m"
  },
  {
    status: "need work",
    column: 5,
    columnColor: Launchpad.Colors.amber,
    hex: "\x1b[33m"
  },
  {
    status: "need information",
    column: 5,
    columnColor: Launchpad.Colors.amber,
    hex: "\x1b[33m"
  },
  {
    status: "quality check",
    column: 7,
    columnColor: Launchpad.Colors.green,
    hex: "\x1b[33m"
  },
  {
    status: "done",
    column: 8,
    hex: "\x1b[32m"
  },
  {
    status: "to prod",
    column: 8,
    hex: "\x1b[32m"
  }
];
