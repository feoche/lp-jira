const Launchpad = require("launchpad-mini");

module.exports = [
  {
    status: "blocker",
    color: Launchpad.Colors.red.full,
    hex: "\x1b[39m"
  },
  {
    status: "critical",
    color: Launchpad.Colors.red.medium,
    hex: "\x1b[39m"
  },
  {
    status: "major",
    color: Launchpad.Colors.red.low,
    hex: "\x1b[31m"
  },
  {
    status: "minor",
    color: Launchpad.Colors.amber.medium,
    hex: "\x1b[33m"
  },
  {
    status: "trivial",
    color: Launchpad.Colors.green.low,
    hex: "\x1b[32m"
  }
];
