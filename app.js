const Launchpad = require("launchpad-mini"),
  pad = new Launchpad(),
  _ = require("lodash"),
  prompt = require("prompt"),
  opn = require("openurl"),
  Watcher = require("rss-watcher"),
  arg = require("minimist")(process.argv.slice(2))._[0],
  exitHook = require("exit-hook"),
  moment = require("moment");

const issueStatus = require("./data/status"),
  issuePriority = require("./data/priority");

let url = arg || "",
  issues = [],
  inter;

if (!url) {
  prompt.start();
  prompt.get(
    {
      properties: {
        URL: {},
        username: {},
        password: {
          hidden: true
        }
      }
    },
    function(err, result) {
      url =
        result.URL +
        "&os_username=" +
        result.username +
        "&os_password=" +
        result.password;
    }
  );
}
pad.connect().then(() => {
  init(pad);

  let rssUrl = url
    .replace(
      /https:\/\/(.*?)\/projects.*?jql(?!Query)/g,
      "https://$1/projects/sr/jira.issueviews:searchrequest-rss/temp/SearchRequest.xml?jqlQuery"
    )
    .replace(/%20/g, "+")
    .replace(/\(/g, "%28")
    .replace(/\)/g, "%29");
  let watcher = new Watcher(rssUrl);

  watcher.interval = 10000;
  watch(watcher, pad);
  setInterval(() => {
    watch(watcher, pad);
  }, 30000);

  let pressDuration = [];
  pad.on("key", k => {
    switch (k.y) {
      case 8: // column buttons
        for (let type of issueStatus) {
          if (k.x === type.column) {
            opn.open(
              url.replace(
                /status%20in%20\(.*?\)/g,
                "status%20in%20(%22" +
                  type.status.split(" ").join("%20") +
                  "%22)"
              )
            );
            break;
          }
        }
        break;
      default:
        let issue = issues.find(issue => {
          return k.x === issue.x && k.y === issue.y;
        });

        if (k.pressed && issue) {
          pressDuration[k] = new Date();

          setTimeout(() => {
            if (
              k.pressed &&
              pressDuration[k] &&
              new Date() - pressDuration[k] > 500
            ) {
              // long press
              console.log(
                issue.type.hex,
                (
                  "[" +
                  issue.type.status
                    .toUpperCase()
                    .match(/\b(\w)/g)
                    .join("") +
                  "]"
                ).padStart(4), // Status
                issue.severity.hex,
                (
                  "[" +
                  issue.severity.status.toUpperCase().substring(0, 2) +
                  "]"
                ).padStart(4), // Severity
                "\x1b[0m",
                issue.title // Title
                /*+ " " + issue.x + " " + issue.y*/
              );
            }
          }, 500);
        }

        if (!k.pressed && issue) {
          if (pressDuration[k] && new Date() - pressDuration[k] > 200) {
            // long press
            pressDuration[k] = 0;
          } else if (pressDuration[k] && new Date() - pressDuration[k] > 50) {
            opn.open(issue.link);
          }
        }
        break;
    }
  });

  watcher.on("error", err => {
    console.info("err : ", err);
    exitWithError(pad);
  });
  exitHook(() => pad.reset(0));
});

function watch(watcher, pad) {
  issues = [];
  watcher.run((err, res) => {
    // if(!res) {
    //   res = require("./mock/mock");
    // }
    if (res) {
      if (inter) {
        clearInterval(inter);
        init(pad);
      }
      res.forEach((item, key) => {
        let i = {};
        for (let type of issueStatus) {
          if (item.description.toLowerCase().match(">" + type.status + "<")) {
            i.type = type;
            break;
          }
        }
        for (let severity of issuePriority) {
          if (item.description.toLowerCase().indexOf(severity.status) > -1) {
            i.severity = severity;
            break;
          }
        }
        if (i.type && i.severity) {
          i.title = item.title;
          i.created = moment(
            item.description
              .match(/Created\:\s(.*?)\s.*?/i)[0]
              .replace(/(Created\:)?\s/i, "")
              .trim(),
            "DD/MMM/YY"
          ).format("DD/MM/YY");
          i.link = item.permalink;
          i.updated = moment(
            item.description
              .match(/&nbsp;Updated\:\s(.*?)\s.*?/i)[0]
              .replace(/(&nbsp;Updated\:)?\s/i, "")
              .trim(),
            "DD/MMM/YY"
          ).format("DD/MM/YY");
          i.link = item.permalink;
          issues.push(i);
        }
      });

      issues = _.sortBy(issues, [
        a => a.type.column,
        a => _.findIndex(issuePriority, ["status", a.severity.status]),
        a => a.created
      ]);

      reset(pad);
      process.stdout.write("\033c");

      let xy = "",
        row = [],
        currentRow = 0,
        currentCol = 0;
      issues.forEach((issue, key) => {
        row[issue.type.column] = row[issue.type.column] || 0;
        if (row[issue.type.column] < 16) {
          currentCol =
            row[issue.type.column] > 7 && issue.type.column === 0
              ? issue.type.column + 1
              : issue.type.column;
          currentRow =
            row[issue.type.column] > 7 && issue.type.column === 8
              ? 9
              : row[issue.type.column] % 8;

          issue.y = currentRow;
          issue.x = currentCol;
          setTimeout(() => {
            paint(pad, issue.severity.color, [issue.x, issue.y]);
          }, 30 * (issue.x + issue.y));

          if (currentCol < issue.type.column + 2) {
            row[issue.type.column] = row[issue.type.column] + 1;
          }
        }

        if(key && issue.type.column !== issues[key-1].type.column) {
          console.info((new Array(100)).fill("— ").join(""));
        }

        console.info(
          issue.type.hex,
          (
            "[" +
            issue.type.status
              .toUpperCase()
              .match(/\b(\w)/g)
              .join("") +
            "]"
          ).padStart(4), // Status
          issue.severity.hex,
          (
            "[" +
            issue.severity.status.toUpperCase().substring(0, 2) +
            "]"
          ).padStart(4), // Severity
          "\x1b[0m",
          ("[" + issue.created + "]").padStart(10), // Date of creation
          ("[" + issue.updated + "]").padStart(10), // Date of update
          "\x1b[0m",
          (issue.title.substring(0,105) + (issue.title.length>105?"…":"")).padEnd(110), // Title
          "\x1b[0m",
          issue.link // Link
        );
      });
      initColumns(pad);
    } else {
      process.stdout.write("\033c");
      console.error("err:", err);
      exitWithError(pad);
    }
  });
}

function paint(pad, color, xy) {
  const intensities = ["low", "medium", "full"];
  if (color) {
    for (let i = 0; i < color._level; i++) {
      setTimeout(() => {
        pad.col(pad[color._name][intensities[i]], xy);
      }, 20 * i);
    }
  }
}

function init(pad) {
  reset(pad);
  let r = 0;
  let i = setInterval(() => {
    if (issues.length) {
      clearInterval(i);
    } else {
      let colors = [];
      for (let j = 0; j < 9; j++) {
        for (let k = 0; k < 9; k++) {
          colors.push([
            (j + r) % 9,
            k,
            j === (9 - k) % 9 ? pad.off : pad.green
          ]);
        }
      }
      pad.setColors(colors);
      r++;
    }
  }, 100);
}

function reset(pad, fullReset) {
  for (let x = 0; x < 9; x++) {
    for (let y = 0; y < (fullReset ? 9 : 8); y++) {
      setTimeout(() => {
        pad.col(pad.off, [x, y]);
      }, 29 * (x + y));
    }
  }
}

function initColumns(pad) {
  pad.col(pad.off, [
    [0, 8],
    [1, 8],
    [2, 8],
    [3, 8],
    [4, 8],
    [5, 8],
    [6, 8],
    [7, 8]
  ]);
  issueStatus.forEach((i, key) => {
    if (i.columnColor) {
      paint(pad, i.columnColor, [i.column, 8]);
    }
  });
}

function exitWithError(pad) {
  pad.reset(0);
  pad.col(
    pad.red,
    pad.fromMap(
      "x-x--x-xo" +
        "-x----x-o" +
        "x-x--x-xo" +
        "--------o" +
        "--------o" +
        "--xxxx--o" +
        "-x----x-o" +
        "--------o" +
        "oooooooo "
    )
  );
  process.exit();
}

exports.pad = pad;
